package com.example.mysmarthome.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mysmarthome.entity.User
import com.example.mysmarthome.entity.UserAndAllEquipments

@Dao
interface UserDao {

    /**
     * add new user
     */
    @Insert
    fun addUser(user: User)

    @Query("SELECT * FROM user WHERE id= :id")
    fun getUserById(id: Int): LiveData<UserAndAllEquipments>


    /**
     * update d'un menu
     */
    @Transaction
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateUserData(user: User)
}