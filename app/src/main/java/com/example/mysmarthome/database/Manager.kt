package com.example.mysmarthome.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.mysmarthome.entity.*


const val DATABASE_NAME = "modulotech"

@Database(entities = [User::class,Address::class, Device::class], version = 1)
@TypeConverters(Converter::class)
abstract class Manager: RoomDatabase() {
    abstract fun equipmentDao() : EquipmentDao
    abstract fun userDao(): UserDao
}
