package com.example.mysmarthome.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mysmarthome.entity.Device

@Dao
interface EquipmentDao {

    /**
     * to load all the equipments by user
     */

    @Transaction
    @Query("SELECT * FROM device ")
    fun loadEquipmentList(): LiveData<List<Device>>


    /**
     * add device to db
     */
    @Insert
    fun addDevice(device: Device)

    /**
     * delete device to db
     */
    @Delete
    fun deleteDevice(device: Device)



    /**
     * to load device by its id
     */
    @Query("SELECT * FROM device WHERE id=:id")
    fun getDeviceById(id: Int): LiveData<Device>


    /**
     * update device
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateDevice(device: Device)


    /**
     * load list of article which content the query string
     */
    @Query("SELECT * FROM device WHERE deviceName LIKE:query")
    fun searchDevice(query:String): LiveData<List<Device>>



}