package com.example.mysmarthome.ui.main.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mysmarthome.R
import com.example.mysmarthome.entity.Device
import com.example.mysmarthome.entity.Type
import kotlinx.android.synthetic.main.device.view.*


class HomeAdapter(var context: Context, var equipmentList: MutableList<Device>, val listener: HomeAdapterListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        private const val TYPE_LIGHT= 0
        private const val TYPE_HEATER = 1
        private const val TYPE_SHUTTER= 2
    }

    interface HomeAdapterListener{
        fun deleteDevice(device: Device)
        fun selectDevice(device: Device)
      /*  fun deleteLightEquipment(light: ProductType.Light)
        fun selectLightEquipment(light: ProductType.Light)
        fun deleteHeaterEquipment(heater: ProductType.Heater)
        fun selectHeaterEquipment(heater: ProductType.Heater)
        fun deleteShutterEquipment(shutter: ProductType.RollerShutter)
        fun selectShutterEquipment(shutter: ProductType.RollerShutter)*/
    }
    private fun deleteDevice(adapterPosition: Int, context: Context?) {
        listener.deleteDevice(equipmentList[adapterPosition])
    }
    private fun selectDevice(adapterPosition: Int, context: Context?) {
        listener.selectDevice(equipmentList[adapterPosition])

    }

  /*  private fun deleteLightEquipment(adapterPosition: Int, context: Context?) {
        listener.deleteLightEquipment(equipmentList[adapterPosition] as ProductType.Light)
    }
    private fun selectLightEquipment(adapterPosition: Int, context: Context?) {
        listener.selectLightEquipment(equipmentList[adapterPosition] as ProductType.Light)

    }
    private fun deleteHeaterEquipment(adapterPosition: Int, context: Context?) {
        listener.deleteHeaterEquipment(equipmentList[adapterPosition] as ProductType.Heater)
    }
    private fun selectHeaterEquipment(adapterPosition: Int, context: Context?) {
        listener.selectHeaterEquipment(equipmentList[adapterPosition] as ProductType.Heater)
    }
    private fun deleteShutterEquipment(adapterPosition: Int, context: Context?) {
        listener.deleteShutterEquipment(equipmentList[adapterPosition] as ProductType.RollerShutter)
    }
    private fun selectShutterEquipment(adapterPosition: Int, context: Context?) {
        listener.selectShutterEquipment(equipmentList[adapterPosition] as ProductType.RollerShutter)
    }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            TYPE_LIGHT -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.device, parent,false)
                val holder = LightEquipmentViewHolder(rootView)
                return holder
            }
            TYPE_HEATER -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.device, parent,false)
                val holder = HeaterEquipmentViewHolder(rootView)
                return holder
            }
            TYPE_SHUTTER -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.device, parent,false)
                val holder = ShutterEquipmentViewHolder(rootView)
                return holder
            }

            else -> throw IllegalArgumentException("Invalid Type view") as Throwable
        }
    }

    override fun getItemCount(): Int {
        return equipmentList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
           is LightEquipmentViewHolder ->holder.fillWithLightEquipment(equipmentList[position])
            is HeaterEquipmentViewHolder ->holder.fillWithHeaterEquipment(equipmentList[position] )
            is ShutterEquipmentViewHolder ->holder.fillWithShutterEquipment(equipmentList[position] )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (equipmentList[position].productType) {
            Type.LIGHT -> TYPE_LIGHT
            Type.HEATER-> TYPE_HEATER
            Type.ROLLERSHUTTER -> TYPE_SHUTTER
            else -> throw IllegalArgumentException("Invalid type of data: $position")
        }
    }




    inner class LightEquipmentViewHolder(rootView: View): RecyclerView.ViewHolder(rootView), View.OnClickListener{

        private val lightEquipmentName = rootView.device_name
        private val lightTitleValue = rootView.title_data
        private val lightImage = rootView.device_image
        private val lightIntensity = rootView.value_data
        private val lightMode = rootView.device_mode
        private val btnDeleteLight = rootView.delete_device_btn
        private val lightContainer = rootView.container_device

        init {
            btnDeleteLight.setOnClickListener(this)
            lightContainer.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v?.id != null){
                when(v.id){
                    R.id.delete_device_btn -> deleteDevice(adapterPosition, v.context)
                    R.id.container_device -> selectDevice(adapterPosition,v.context)
                }
            }
        }

        fun fillWithLightEquipment(device: Device){
            lightEquipmentName.text = device.deviceName
            lightTitleValue.text = context.resources.getString(R.string.intensity)
            lightImage.setImageResource(R.drawable.light)
            lightIntensity.text = device.intensity.toString()

            if (device.mode == "ON"){
                lightMode.setImageResource(R.drawable.switch_on)
            } else{
                lightMode.setImageResource(R.drawable.switch_off)
            }
        }

    }

    inner class HeaterEquipmentViewHolder(rootView: View): RecyclerView.ViewHolder(rootView), View.OnClickListener{

        private val heaterEquipmentName = rootView.device_name
        private val heaterTitleValue = rootView.title_data
        private val heaterImage = rootView.device_image
        private val heaterTemperature = rootView.value_data
        private val heaterMode = rootView.device_mode
        private val btnDeleteHeater = rootView.delete_device_btn
        private val heaterContainer = rootView.container_device

        init {
            btnDeleteHeater.setOnClickListener(this)
            heaterContainer.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v?.id != null){
                when(v.id){
                    R.id.delete_device_btn-> deleteDevice(adapterPosition, v.context)
                    R.id.container_device-> selectDevice(adapterPosition,v.context)
                }
            }
        }

        fun fillWithHeaterEquipment(heater: Device){
            heaterEquipmentName.text = heater.deviceName
            heaterTitleValue.text = "température : "
            heaterImage.setImageResource(R.drawable.heater)
            heaterTemperature.text = context.resources.getString(R.string.degrees,heater.temperature.toString())

            if (heater.mode == "ON"){
                heaterMode.setImageResource(R.drawable.switch_on)
            } else{
                heaterMode.setImageResource(R.drawable.switch_off)
            }
        }

    }

    inner class ShutterEquipmentViewHolder(rootView: View): RecyclerView.ViewHolder(rootView), View.OnClickListener{

        private val shutterEquipmentName = rootView.device_name
        private val shutterTitleValue = rootView.title_data
        private val shutterPosition = rootView.value_data
        private val btnDeleteShutter = rootView.delete_device_btn
        private val shutterContainer = rootView.container_device
        private val shutterImage = rootView.device_image
        private val shutterMode = rootView.device_mode

        init {
            btnDeleteShutter.setOnClickListener(this)
            shutterContainer.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v?.id != null){
                when(v.id){
                    R.id.delete_device_btn -> deleteDevice(adapterPosition, v.context)
                    R.id.container_device -> selectDevice(adapterPosition,v.context)
                }
            }
        }

        fun fillWithShutterEquipment(shutter: Device){
            shutterEquipmentName.text = shutter.deviceName
            shutterTitleValue.text = context.resources.getString(R.string.position)
            shutterPosition.text = shutter.position.toString()
            shutterImage.setImageResource(R.drawable.shutter)
            shutterMode.visibility =View.INVISIBLE

        }

    }

}