package com.example.mysmarthome.ui.main.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mysmarthome.R
import com.example.mysmarthome.entity.Address
import com.example.mysmarthome.entity.User
import com.example.mysmarthome.entity.UserAndAllEquipments
import com.example.mysmarthome.utils.LocalPreferences
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_user.*


class UserFragment : Fragment() {

    private lateinit var viewModel: UserViewModel
    private lateinit var currentUser: User
    private var userId = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if ( LocalPreferences(this.requireContext()).id != null){
            userId = LocalPreferences(this.requireContext()).id!!.toInt()
            val factory = UserFragmentFactory(userId)
            viewModel = ViewModelProvider(this, factory).get(UserViewModel::class.java)
            viewModel.userId.observe(viewLifecycleOwner, Observer { user -> displaySavedUser(user) })
        } else {
            val factory = UserFragmentFactory(userId)
            viewModel = ViewModelProvider(this, factory).get(UserViewModel::class.java)
            viewModel.loadUser()
            viewModel.user.observe(viewLifecycleOwner, Observer { user -> displayUser(user) })
        }

    }

    private fun displayUser(user: UserAndAllEquipments) {
        if (user != null) {
            currentUser = user.user!!
            firstName_txt.text = currentUser!!.firstName
            lastName_txt.text = currentUser!!.lastName
            birthdate_txt.text = currentUser!!.birthDate.toString()
            street_code_input.setText(currentUser!!.address.streetCode)
            street_input.setText(currentUser!!.address.street)
            city_input.setText(currentUser!!.address.city)
            zip_code_input.setText(currentUser!!.address.postalCode.toString())
            viewModel.addNewUser(currentUser)
            user!!.user!!.id = 1
            currentUser.id = user!!.user!!.id
            LocalPreferences(this.requireContext()).id = currentUser.id.toString()
            LocalPreferences(this.requireContext()).firstName = currentUser.firstName
        }
    }

    private fun displaySavedUser(user: UserAndAllEquipments) {
        if (user != null) {
            currentUser = user.user!!
            firstName_txt.text = currentUser!!.firstName
            lastName_txt.text = currentUser!!.lastName
            birthdate_txt.text = currentUser!!.birthDate.toString()
            street_code_input.setText(currentUser!!.address.streetCode)
            street_input.setText(currentUser!!.address.street)
            city_input.setText(currentUser!!.address.city)
            zip_code_input.setText(currentUser!!.address.postalCode.toString())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_user, container, false)
        val editFab = root.findViewById<FloatingActionButton>(R.id.editBtn)
        editFab.setOnClickListener {
            editUserData()
        }
        return root
    }

    private fun editUserData() {
        val user = User(
            id= currentUser.id,
            firstName = currentUser.firstName,
            lastName = currentUser.lastName,
            birthDate = currentUser.birthDate,
            address = Address(streetCode = street_code_input.text.toString(),
                street = street_input.text.toString(),city = city_input.text.toString(),
                postalCode = zip_code_input.text.toString().toInt(), country = currentUser.address.country, addressId = currentUser.address.addressId)
        )
        viewModel.updateUser(user)
        Toast.makeText(this.context,"Profil mis à jour", Toast.LENGTH_SHORT).show()
    }

}
