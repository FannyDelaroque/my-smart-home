package com.example.mysmarthome.ui.main.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.mysmarthome.App
import com.example.mysmarthome.entity.User
import com.example.mysmarthome.entity.UserAndAllEquipments
import java.util.concurrent.Executors

class UserViewModel(id:Int) : ViewModel() {

    private val _user = MutableLiveData<UserAndAllEquipments>()
    val user: LiveData<UserAndAllEquipments> = _user


    private val _userId = MutableLiveData<Int>()
    val userId: LiveData<UserAndAllEquipments> =  Transformations.switchMap(_userId){
            id -> App.database.userDao().getUserById(id)
    }

    init {
        _userId.value = id
    }

    fun addNewUser(user: User){
        Executors.newSingleThreadExecutor().execute {
            App.database.userDao().addUser(user)
        }
    }
    fun updateUser(user: User){
        Executors.newSingleThreadExecutor().execute {
            App.database.userDao().updateUserData(user)
        }
    }

    fun loadUser(){
        App.repository.getUser(){
                user, error ->  _user.value = user
        }
    }
}