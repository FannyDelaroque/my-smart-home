package com.example.mysmarthome.ui.splashscreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mysmarthome.R
import com.example.mysmarthome.ui.main.MainActivity

class SplashscreenActivity :AppCompatActivity() {

    /**
     * app with 5 screens
     * 1 : splashscreen
     * 2: list of connected devices (homeFragment)
     * 3: detail of selected device (deviceActivity)
     * 4: user data (userFragment)
     * 5: my data (aboutMeFragment)
     */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /**
         * display splashscreen.xml in drawable file
         * it launches HomeFragment, a fragment of MainActivity
         */
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()

    }
}
