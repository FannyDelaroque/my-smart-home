package com.example.mysmarthome.ui.device

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mysmarthome.R
import com.example.mysmarthome.entity.Device
import com.example.mysmarthome.entity.Type
import kotlinx.android.synthetic.main.activity_device.*


class DeviceActivity : AppCompatActivity(), DeviceAdapter.DeviceAdapterListener {

    private var lightId : Int = 0
    private var heaterId : Int = 0
    private var shutterId : Int = 0
    private var deviceId:Int=0
    private lateinit var viewModel: DeviceViewModel
    private lateinit var devices: MutableList<Device>
    private lateinit var recyclerView:RecyclerView
    private lateinit var adapter: DeviceAdapter

    companion object {
        const val LIGHT_ID = "light_id"
        const val HEATER_ID = "heater_id"
        const val SHUTTER_ID = "roller_shutter_id"
        const val DEVICE_ID = "device_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device)

        lightId = intent.getIntExtra(LIGHT_ID, 0)
        heaterId = intent.getIntExtra(HEATER_ID, 0)
        shutterId = intent.getIntExtra(SHUTTER_ID, 0)
        deviceId = intent.getIntExtra(DEVICE_ID,0)
        devices = mutableListOf()

        if(deviceId != 0){
            val factory = DeviceActivityFactory(deviceId)
            viewModel = ViewModelProvider(this, factory).get(DeviceViewModel::class.java)
            viewModel.deviceId.observe(this, Observer { device ->display(device) })
        }

    }


    private fun display(device: Device) {
        if (device != null) {
            devices.clear()
            devices.add(device)
            adapter = DeviceAdapter(this, this,devices)
            adapter.notifyDataSetChanged()
            recyclerView = recycler_detail_device as RecyclerView
            recyclerView!!.layoutManager = LinearLayoutManager(this)
            recyclerView!!.adapter =adapter
        }

    }

    override fun updateDevice(device: Device,mode: String, intensity: Int,temp:Double, position: Int) {
        var deviceToUpdate: Device ?= null
        if(device.productType == Type.LIGHT){
            deviceToUpdate = Device(
                id = device.id,
                deviceName= device.deviceName,
                productType = device.productType,
                mode= mode,
                intensity= intensity,
                position = null,
                temperature = null)
        }
        if (device.productType == Type.HEATER){
            deviceToUpdate = Device(
                id = device.id,
                deviceName= device.deviceName,
                productType = device.productType,
                mode= mode,
                intensity= null,
                position = null,
                temperature = temp)
        }
        if(device.productType == Type.ROLLERSHUTTER){
            deviceToUpdate = Device(
                id = device.id,
                deviceName= device.deviceName,
                productType = device.productType,
                mode= null,
                intensity= null,
                position = position,
                temperature = null)
        }
        if (deviceToUpdate != null) {
            viewModel.updateDevice(deviceToUpdate)
        }
    }

}
