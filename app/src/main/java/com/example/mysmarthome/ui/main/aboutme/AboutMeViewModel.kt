package com.example.mysmarthome.ui.main.aboutme

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AboutMeViewModel : ViewModel() {

    private val _firstName = MutableLiveData<String>().apply {
        value = "Fanny"
    }
    val firstName: LiveData<String> = _firstName

    private val _lastName = MutableLiveData<String>().apply {
        value = "Delaroque"
    }
    val lastName: LiveData<String> = _lastName

    private val _birthDate = MutableLiveData<String>().apply {
        value = "19/04/1989"
    }
    val birthdate: LiveData<String> = _birthDate

    private val _email = MutableLiveData<String>().apply {
        value = "fanny.delaroque@gmail.com"
    }
    val email: LiveData<String> = _email

    private val _phoneNumber = MutableLiveData<String>().apply {
        value = "06 88 15 90 04"
    }
    val phoneNumber: LiveData<String> = _phoneNumber

    private val _portfolio = MutableLiveData<String>().apply {
        value = "<a href='https://fannydelaroque.wixsite.com/portfolio'> Consulter mon portfolio en ligne</a>"
    }
    val portfolio: LiveData<String> = _portfolio
}