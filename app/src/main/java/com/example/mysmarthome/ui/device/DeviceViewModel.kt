package com.example.mysmarthome.ui.device

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.mysmarthome.App
import com.example.mysmarthome.entity.Device
import java.util.concurrent.Executors

class DeviceViewModel(id:Int) : ViewModel() {


    fun updateDevice(device: Device) {
        Executors.newSingleThreadExecutor().execute {
            App.database.equipmentDao().updateDevice(device)
        }
    }

    private val _deviceId = MutableLiveData<Int>()


    val deviceId : LiveData<Device> =  Transformations.switchMap(_deviceId){
            id -> App.database.equipmentDao().getDeviceById(id)
    }

    init {
        _deviceId.value = id
    }
}