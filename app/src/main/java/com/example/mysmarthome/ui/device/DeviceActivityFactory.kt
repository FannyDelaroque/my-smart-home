package com.example.mysmarthome.ui.device

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class DeviceActivityFactory  (private  val  id: Int): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DeviceViewModel(id) as T
    }
}