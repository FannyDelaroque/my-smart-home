package com.example.mysmarthome.ui.main.user

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class UserFragmentFactory (private  val  id: Int): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserViewModel(id) as T
    }
}