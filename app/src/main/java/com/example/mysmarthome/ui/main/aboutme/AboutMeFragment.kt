package com.example.mysmarthome.ui.main.aboutme

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mysmarthome.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_about_me.*


class AboutMeFragment : Fragment() {

    private lateinit var viewModel: AboutMeViewModel
    private lateinit var email:String


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProvider(this).get(AboutMeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_about_me, container, false)

        viewModel.firstName.observe(viewLifecycleOwner, Observer { firstName_txt.text = it })
        viewModel.lastName.observe(viewLifecycleOwner, Observer { lastName_txt.text = it })
        viewModel.birthdate.observe(viewLifecycleOwner, Observer { birthdate_txt.text = it })
        viewModel.phoneNumber.observe(viewLifecycleOwner, Observer { phone_number_textView.text = it })
        viewModel.email.observe(viewLifecycleOwner, Observer {
            email_txt.text = it
            email = it
        })
        val portfolio = root.findViewById<TextView>(R.id.portfolio_textView)
        viewModel.portfolio.observe(viewLifecycleOwner, Observer {
            portfolio.isClickable = true
            portfolio.movementMethod = LinkMovementMethod.getInstance()
            portfolio.text = Html.fromHtml(it,Html.FROM_HTML_MODE_COMPACT);
        })

        val sendEmailFab = root.findViewById<FloatingActionButton>(R.id.sendEmailBtn)
        sendEmailFab.setOnClickListener {
            sendEmail()
        }
        return root
    }

    private fun sendEmail() {
            /*ACTION_SEND action to launch an email client installed on your Android device.*/
            val mIntent = Intent(Intent.ACTION_SEND)
            mIntent.data = Uri.parse("mailto:")
            mIntent.type = "text/plain"
            mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))

            try {
                //start email intent
                startActivity(Intent.createChooser(mIntent, ""))
            }
            catch (e: Exception){
                //if any thing goes wrong for example no email client application or any exception
                //get and show exception message
                Toast.makeText(this.requireContext(), e.message, Toast.LENGTH_LONG).show()
            }

    }
}
