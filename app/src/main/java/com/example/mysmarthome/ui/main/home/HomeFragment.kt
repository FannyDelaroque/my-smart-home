package com.example.mysmarthome.ui.main.home

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mysmarthome.R
import com.example.mysmarthome.entity.*
import com.example.mysmarthome.ui.device.DeviceActivity
import com.example.mysmarthome.ui.device.DeviceActivityFactory
import com.example.mysmarthome.ui.device.DeviceViewModel
import com.example.mysmarthome.utils.LocalPreferences
import kotlinx.android.synthetic.main.fragment_home.*



class HomeFragment : Fragment(), HomeAdapter.HomeAdapterListener, SearchView.OnQueryTextListener,
    MenuItem.OnActionExpandListener, SearchView.OnCloseListener {

    private lateinit var homeViewModel: HomeViewModel
    private var recyclerView: RecyclerView? = null
    private lateinit var devicesList: MutableList<Device>
    private lateinit var myDevicesList: MutableList<Device>
    private lateinit var adapter: HomeAdapter
    private lateinit var searchView: androidx.appcompat.widget.SearchView
    private lateinit var searchItem: MenuItem
    private lateinit var research: String

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, menuInflater)
        menuInflater.inflate(R.menu.main, menu)
        /**
         * to search a device by its name
         */
        searchItem = menu?.findItem(R.id.action_settings)
        searchView = searchItem?.actionView as androidx.appcompat.widget.SearchView
        searchView.setOnQueryTextListener(this)
        searchView.setOnCloseListener(this)
        searchItem.setOnActionExpandListener(this)

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /**
         * subscription to the change in devices list
         */

        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        homeViewModel.equipments.observe(viewLifecycleOwner, Observer { list -> displaySavedList(list) })

    }




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_home, container, false)
        devicesList = mutableListOf()
        myDevicesList = mutableListOf()


        return root
    }

    /**
     * for the first launch, I recorded datas in Room db because I didn't want to use SharedPreferences to
     * save/update the devices's informations
     */
    private fun display(list: DeviceList) {


            myDevicesList.clear()
            myDevicesList.addAll(list.result)
            for (device in list.result){
                homeViewModel.addDevice(device)
            }
            adapter = HomeAdapter(this.requireContext(),myDevicesList,this)
            recyclerView = recycler_equipment as RecyclerView
            recyclerView!!.layoutManager = LinearLayoutManager(this.requireContext())
            recyclerView!!.adapter =adapter

    }
   private fun displaySavedList(list: List<Device>) {
        if(list.isNotEmpty()) {
            myDevicesList.clear()
            myDevicesList.addAll(list)
            adapter = HomeAdapter(this.requireContext(),myDevicesList,this)
            recyclerView = recycler_equipment as RecyclerView
            recyclerView!!.layoutManager = LinearLayoutManager(this.requireContext())
            recyclerView!!.adapter =adapter
        } else {
            homeViewModel.loadDeviceList()
            homeViewModel.deviceList.observe(viewLifecycleOwner, Observer { list -> display(list) })
            }

        }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (query != null) {
            getListFromDatabase(query)
            searchView.clearFocus()
        }
            return true
    }

    private fun getListFromDatabase(query: String) {
        var searchText = query
        searchText = "%$searchText%"
        homeViewModel.loadSearchDevice(searchText).observe(viewLifecycleOwner, Observer { query ->displaySavedList(query) })


    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        homeViewModel.loadSavedList().observe(viewLifecycleOwner, Observer { list ->displaySavedList(list) })
        return true
    }

    override fun deleteDevice(device: Device) {
        homeViewModel.deleteDevice(device)
    }

    override fun selectDevice(device: Device) {
        val intent = Intent(this.context, DeviceActivity::class.java)
        intent.putExtra(DeviceActivity.DEVICE_ID, device.id)
        startActivity(intent)
    }

    override fun onClose(): Boolean {
        homeViewModel.loadSavedList()
        searchView.onActionViewCollapsed()
        return true
    }


}
