package com.example.mysmarthome.ui.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.mysmarthome.App
import com.example.mysmarthome.entity.*
import java.util.concurrent.Executors


class HomeViewModel: ViewModel() {

   var equipments = App.database.equipmentDao().loadEquipmentList()

    private val _deviceList: MutableLiveData<DeviceList> = MutableLiveData()
    val deviceList: LiveData<DeviceList> = _deviceList



    fun loadDeviceList(){
        App.repository.getDeviceList(){
                list, error ->  _deviceList.value = list
        }
    }

    fun loadSavedList():LiveData<List<Device>>  {
        equipments= App.database.equipmentDao().loadEquipmentList()
        return equipments
    }

    fun deleteDevice(device: Device){
        Executors.newSingleThreadExecutor().execute {
            App.database.equipmentDao().deleteDevice(device)
        }
    }
    fun addDevice(device: Device){
        Executors.newSingleThreadExecutor().execute {
            App.database.equipmentDao().addDevice((device))
        }
    }


    fun loadSearchDevice (query:String):LiveData<List<Device>> {
        equipments = App.database.equipmentDao().searchDevice(query)
        return equipments

    }

}