package com.example.mysmarthome.ui.device

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.mysmarthome.R
import com.example.mysmarthome.entity.Device
import com.example.mysmarthome.entity.Type
import kotlinx.android.synthetic.main.detail_heater.view.*
import kotlinx.android.synthetic.main.detail_light.view.*
import kotlinx.android.synthetic.main.detail_shutter.view.*

class DeviceAdapter(var context: Context, var listener: DeviceAdapterListener, var deviceList:MutableList<Device>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var redColor = ContextCompat.getColor(context, R.color.red);
    private var greenColor = ContextCompat.getColor(context, R.color.green);
    interface DeviceAdapterListener {
        fun updateDevice(device: Device,mode: String, intensity: Int,temp:Double, position: Int)

    }

    companion object{
        private const val TYPE_LIGHT= 0
        private const val TYPE_HEATER = 1
        private const val TYPE_SHUTTER= 2
    }

    private fun updateDevice(index: Int, context: Context, mode: String, intensity: Int,temp:Double, position: Int) {
        listener.updateDevice(deviceList[index],mode, intensity,temp, position)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            TYPE_LIGHT -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.detail_light, parent,false)
                val holder = LightDeviceViewHolder(rootView)
                return holder
            }
            TYPE_HEATER -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.detail_heater, parent,false)
                val holder = HeaterDeviceViewHolder(rootView)
                return holder
            }
            TYPE_SHUTTER -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.detail_shutter, parent,false)
                val holder = ShutterDeviceViewHolder(rootView)
                return holder
            }

            else -> throw IllegalArgumentException("Invalid Type view") as Throwable
        }
    }

    override fun getItemCount(): Int {
        return deviceList.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (deviceList[position].productType) {
            Type.LIGHT-> TYPE_LIGHT
            Type.HEATER-> TYPE_HEATER
            Type.ROLLERSHUTTER -> TYPE_SHUTTER
            else -> throw IllegalArgumentException("Invalid type of data: $position")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is LightDeviceViewHolder ->holder.fillWithLightDevice(deviceList[position] )
            is HeaterDeviceViewHolder ->holder.fillWithHeaterDevice(deviceList[position])
            is ShutterDeviceViewHolder ->holder.fillWithShutterDevice(deviceList[position])
        }
    }

    inner class LightDeviceViewHolder(rootView: View): RecyclerView.ViewHolder(rootView), View.OnClickListener,
        SeekBar.OnSeekBarChangeListener {
        private val lightDeviceName = rootView.light_device_name
        private val lightIntensity = rootView.light_value_txt
        private val lightMode = rootView.light_mode_value
        private val lightPowerBtn = rootView.btnPowerLight
        private val seekBar = rootView.seekBarLight


        init {
            lightPowerBtn.setOnClickListener(this)
            seekBar.setOnSeekBarChangeListener(this)
        }

        override fun onClick(v: View?) {
            if(v?.id != null){
                when(v.id){
                    R.id.btnPowerLight -> changeLightMode()
                }
            }
        }

        private fun changeLightMode()  {
            if(lightMode.text == "ON"){
            lightMode.text = "OFF"
        } else{
            lightMode.text = "ON"
        }
        updateDevice(adapterPosition,context,lightMode.text.toString(),lightIntensity.text.toString().toInt(),0.0,0)

        }


        fun fillWithLightDevice(light: Device){
            lightDeviceName.text = light.deviceName
            lightIntensity.text = light.intensity.toString()
            lightMode.text = light.mode
            seekBar.progress = light.intensity!!

            if(lightMode.text == "ON"){
                ImageViewCompat.setImageTintList(lightPowerBtn, ColorStateList.valueOf(greenColor))
            } else{
                ImageViewCompat.setImageTintList(lightPowerBtn, ColorStateList.valueOf(redColor))
            }
        }

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            lightIntensity.text = seekBar?.progress.toString()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            lightIntensity.text = seekBar?.progress.toString()
            updateDevice(adapterPosition, context,lightMode.text.toString(),lightIntensity.text.toString().toInt(),0.0,0)
        }

    }




    inner class HeaterDeviceViewHolder(rootView: View): RecyclerView.ViewHolder(rootView), View.OnClickListener{
        private val heaterDeviceName = rootView.heater_device_name
        private val heaterTemperature = rootView.temperature_value_txt
        private val heaterMode = rootView.heater_mode_value
        private val heaterPowerBtn = rootView.btnPowerHeater
        private val minusBtn = rootView.btnDecrement
        private val plusBtn = rootView.btnIncrement
        private var count = 0.0
        private  var  temperature : Double ?=null
        private  var  newTemperature = 0.0
        private lateinit var mode : String



        init {
            minusBtn.setOnClickListener(this)
            plusBtn.setOnClickListener(this)
            heaterPowerBtn.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if(v?.id != null){
                when(v.id){
                    R.id.btnIncrement -> increaseTemperature()
                    R.id.btnDecrement -> decreaseTemperature()
                    R.id.btnPowerHeater -> changeHeaterMode()
                }
            }
        }

        private fun changeHeaterMode() {
            if(heaterMode.text == "ON"){
                heaterMode.text = "OFF"
            } else{
                heaterMode.text = "ON"
            }
            updateDevice(adapterPosition,context,heaterMode.text.toString(),0,heaterTemperature.text.toString().toDouble(),0)
        }

        private fun decreaseTemperature() {
            if(temperature!! > 7){
                count -= 0.5
                newTemperature = temperature!! + count
                heaterTemperature.text = newTemperature.toString()
                updateDevice(adapterPosition,context, heaterMode.text.toString(),0,newTemperature!!,0)
            }
        }

        private fun increaseTemperature() {
            if(temperature!! < 28){
                count += 0.5
                newTemperature = temperature!! + count
                heaterTemperature.text = newTemperature.toString()
                updateDevice(adapterPosition,context, heaterMode.text.toString(),0,newTemperature!!,0)

            }
        }

        fun fillWithHeaterDevice(heater: Device){
            heaterDeviceName.text = heater.deviceName
            temperature = heater.temperature
            heaterTemperature.text = temperature.toString()
            heaterMode.text = heater.mode


            if(heaterMode.text == "ON"){
                ImageViewCompat.setImageTintList(heaterPowerBtn, ColorStateList.valueOf(greenColor))
            }else{
                ImageViewCompat.setImageTintList(heaterPowerBtn, ColorStateList.valueOf(redColor))
            }

        }

    }



    inner class ShutterDeviceViewHolder(rootView: View): RecyclerView.ViewHolder(rootView),
        SeekBar.OnSeekBarChangeListener {
        private val shutterDeviceName = rootView.shutter_device_name
        private val position = rootView.shutter_position_text
        private val seekBar = rootView.shutter_seekbar


        init {
            seekBar.setOnSeekBarChangeListener(this)
        }

        fun fillWithShutterDevice(shutter: Device){
            shutterDeviceName.text = shutter.deviceName
            position.text = shutter.position.toString()
            seekBar.progress = shutter.position!!
        }

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            position.text = seekBar?.progress.toString()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            position.text = seekBar?.progress.toString()
            updateDevice(adapterPosition,context,"",0,0.0,position.text.toString().toInt())
        }

    }




}