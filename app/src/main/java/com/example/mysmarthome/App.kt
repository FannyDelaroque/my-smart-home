package com.example.mysmarthome

import android.app.Application
import androidx.room.Room
import com.example.mysmarthome.database.DATABASE_NAME
import com.example.mysmarthome.database.Manager
import com.example.mysmarthome.repository.Repository
import java.util.concurrent.Executors

class App: Application(){

    companion object{
        lateinit var database : Manager
        lateinit var repository:Repository
        }

    override fun onCreate() {
        super.onCreate()

        database = Room.databaseBuilder(this, Manager::class.java, DATABASE_NAME)
            .build()
        Executors.newSingleThreadExecutor().execute {
            database.equipmentDao().loadEquipmentList()
        }
        repository = Repository()
    }
}