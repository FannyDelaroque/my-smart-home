package com.example.mysmarthome.utils

import android.content.Context
import android.preference.PreferenceManager

class LocalPreferences (context: Context) {
    val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    var firstName : String?
        get() {
            return sharedPreferences.getString("firstName", null)
        }
        set(value) {
            sharedPreferences.edit().putString("firstName",value).apply()
        }

    var id : String?
        get() {
            return sharedPreferences.getString("id", null)
        }
        set(value) {
            sharedPreferences.edit().putString("id",value).apply()
        }

}