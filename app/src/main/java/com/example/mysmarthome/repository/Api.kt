package com.example.mysmarthome.repository

import com.example.mysmarthome.entity.DeviceList
import com.example.mysmarthome.entity.User
import com.example.mysmarthome.entity.UserAndAllEquipments
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {


        @Headers("Content-Type: application/json")
        @GET("data.json")
        fun getDeviceList(): Call<DeviceList>


        @Headers("Content-Type: application/json")
        @GET("data.json")
        fun getSearchDevice(@Query("query")query:String): Call<DeviceList>

        @Headers("Content-Type: application/json")
        @GET("data.json")
        fun getUser(): Call<UserAndAllEquipments>


}