package com.example.mysmarthome.repository

import android.service.carrier.CarrierMessagingService
import android.util.Log
import com.example.mysmarthome.entity.DeviceList
import com.example.mysmarthome.entity.User
import com.example.mysmarthome.entity.UserAndAllEquipments
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository() {
    private val service = RetrofitBuilder.buildService(Api::class.java)

    fun getDeviceList(resultHandler: (deviceList: DeviceList, error: String?) -> Unit) {
        var list: DeviceList

        service.getDeviceList().enqueue(object : Callback<DeviceList> {
            override fun onFailure(call: Call<DeviceList>, t: Throwable) {
                Log.d("RepositoryDeviceList", t.message.toString())

            }

            override fun onResponse(call: Call<DeviceList>, response: Response<DeviceList>) {
                if (response.isSuccessful) {
                    list = response.body()!!
                    resultHandler(list, null)

                }
                Log.d("RepositoryOnResponse", response.body().toString())

            }
        })
    }

    fun getSearchDevice(
        query: String,
        resultHandler: (deviceList: DeviceList, error: String?) -> Unit) {
        var deviceList: DeviceList

        service.getSearchDevice(query).enqueue(object : Callback<DeviceList> {
            override fun onFailure(call: Call<DeviceList>, t: Throwable) {
                Log.d("RepositorySearch", t.message.toString())

            }

            override fun onResponse(call: Call<DeviceList>, response: Response<DeviceList>) {
                if (response.isSuccessful) {
                    deviceList = response.body()!!
                    resultHandler(deviceList, null)

                }
                Log.d("RepositoryOnResponse", response.body().toString())

            }
        })
    }

    fun getUser(resultHandler: (user:UserAndAllEquipments, error: String?) -> Unit){
        var user:UserAndAllEquipments
        service.getUser().enqueue(object :Callback<UserAndAllEquipments>{
            override fun onFailure(call: Call<UserAndAllEquipments>, t: Throwable) {
                Log.d("RepositoryGetUser", t.message.toString())
            }

            override fun onResponse(call: Call<UserAndAllEquipments>, response: Response<UserAndAllEquipments>) {
                if (response.isSuccessful) {

                    user = response.body()!!
                    resultHandler(user, null)
                }
                Log.d("RepositoryOnResponse", response.body().toString())
            }
        })
    }

}