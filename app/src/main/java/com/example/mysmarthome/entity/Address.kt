package com.example.mysmarthome.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity (tableName = "address", indices = [Index(value = ["address_id"], unique = true)])
data class Address(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "address_id")
    val addressId : Int,
    @SerializedName("city")
    val city: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("postalCode")
    val postalCode: Int,
    @SerializedName("street")
    val street: String,
    @SerializedName("streetCode")
    val streetCode: String
)