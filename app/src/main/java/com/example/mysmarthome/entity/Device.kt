package com.example.mysmarthome.entity

import androidx.room.*
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

@TypeConverters(Converter::class)
@Entity(tableName = "device")
data class Device(
    @PrimaryKey
    @SerializedName("id")
     val id: Int,
    @SerializedName("deviceName")
     val deviceName: String,
    @SerializedName("productType")
     val productType: Type,
    @SerializedName("mode")
     val mode: String?,
    @SerializedName("intensity")
     val intensity: Int?,
    @SerializedName("position")
     val position: Int?,
    @SerializedName("temperature")
     val temperature: Double?) {
}


enum class Type(val type: String) {
    @SerializedName("Light")
    LIGHT("Light"),

    @SerializedName("Heater")
    HEATER("Heater"),

    @SerializedName("RollerShutter")
    ROLLERSHUTTER("RollerShutter")
}

    class Converter {
        companion object {
            @JvmStatic
            @TypeConverter
            fun fromEnum(data: Type): String? {
            /*    return when (data) {
                    null -> null
                    else -> data.type
                }*/
                return Gson().toJson(data)
            }

            @JvmStatic
            @TypeConverter
            fun toString(s: String): Type =
                Gson().fromJson(s, Type::class.java)
        }
}
