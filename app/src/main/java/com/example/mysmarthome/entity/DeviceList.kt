package com.example.mysmarthome.entity

import com.google.gson.annotations.SerializedName

data class DeviceList (
    @SerializedName("devices")
    val result: List<Device>
)