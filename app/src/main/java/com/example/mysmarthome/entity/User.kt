package com.example.mysmarthome.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "user", indices = [Index(value = ["id"], unique = true)])
data class User(
    @PrimaryKey(autoGenerate = false)
    var id:Int,
    @Embedded
    @SerializedName("address")
    val address: Address,
    @SerializedName("birthDate")
    val birthDate: Long,
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("lastName")
    val lastName: String
): Serializable